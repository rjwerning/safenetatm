﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SafeNetATM.Helpers;
using SafeNetATM.Models;

namespace SafeNetATM.Test {
    [TestClass]
    public class UnitTest1 {
        // Total value = 1860
        List<Denomination> denominations = new List<Denomination>() {
            new Denomination { Value = 1, InitialQty = 10, RemainingQty = 10 },
            new Denomination { Value = 5, InitialQty = 10, RemainingQty = 10 },
            new Denomination { Value = 10, InitialQty = 10, RemainingQty = 10 },
            new Denomination { Value = 20, InitialQty = 10, RemainingQty = 10 },
            new Denomination { Value = 50, InitialQty = 10, RemainingQty = 10 },
            new Denomination { Value = 100, InitialQty = 10, RemainingQty = 10 }
        };

        private Denomination GetByValue(int value) {
            return denominations.Find(x => x.Value == value);
        }

        [TestMethod]
        public void TestWithdraw() {
            // Order list Desc, same as all queries in controller should be
            denominations = denominations.OrderByDescending(x => x.Value).ToList();
            Denomination item;
            int remainder = 988;
            DenominationUtils.Withdraw(denominations, ref remainder);
            Assert.AreEqual(remainder, 0);
            item = GetByValue(100);
            Assert.AreEqual(item.RemainingQty, 1);
            item = GetByValue(50);
            Assert.AreEqual(item.RemainingQty, 9);
            item = GetByValue(20);
            Assert.AreEqual(item.RemainingQty, 9);
            item = GetByValue(10);
            Assert.AreEqual(item.RemainingQty, 9);
            item = GetByValue(5);
            Assert.AreEqual(item.RemainingQty, 9);
            item = GetByValue(1);
            Assert.AreEqual(item.RemainingQty, 7);


            remainder = 988;
            Assert.AreNotEqual(remainder, 0);
        }

        [TestMethod]
        public void Restock() {
            DenominationUtils.Restock(denominations);
            foreach (Denomination item in denominations)
                Assert.AreEqual(item.RemainingQty, item.InitialQty);
        }
    }
}
