﻿using SafeNetATM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SafeNetATM.Helpers {
    public static class DenominationUtils {
        public static void Restock(List<Denomination> denominations) {
            foreach (Denomination item in denominations)
                item.RemainingQty = item.InitialQty;
        }

        // Don't have a way to do a rollback for the memory table, so updates to the remainingqty end up
        // modifying the actual value even if it's going to fail.  So need this validation check
        public static bool HasEnoughFunds(List<Denomination> denominations, int remainder) {
            foreach (Denomination item in denominations) {
                if (remainder == 0)
                    break;

                if (remainder > item.Value && item.RemainingQty > 0) {
                    int amt = remainder / item.Value;
                    if (amt > item.RemainingQty)
                        amt = item.RemainingQty;

                    remainder -= amt * item.Value;
                }
            }

            return remainder == 0;
        }

        public static void Withdraw(List<Denomination> denominations, ref int remainder) {
            foreach (Denomination item in denominations) {
                if (remainder == 0)
                    break;

                if (remainder > item.Value && item.RemainingQty > 0) {
                    int amt = remainder / item.Value;
                    if (amt > item.RemainingQty)
                        amt = item.RemainingQty;

                    remainder -= amt * item.Value;
                    item.RemainingQty -= amt;
                }
            }
        }
    }
}