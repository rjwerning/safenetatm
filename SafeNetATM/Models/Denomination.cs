﻿namespace SafeNetATM.Models {
    public class Denomination: BaseModel {
        public int Value { get; set; }
        public int InitialQty { get; set; }
        public int RemainingQty { get; set; }
    }
}