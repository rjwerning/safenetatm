﻿using System.ComponentModel.DataAnnotations;

namespace SafeNetATM.Models {
    public abstract class BaseModel {
        [Key]
        public int Id { get; set; }
    }
}