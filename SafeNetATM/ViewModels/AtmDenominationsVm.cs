﻿using System.Collections.Generic;

namespace SafeNetATM.ViewModels {
    public class AtmDenominationsVm {
        public string Result { get; set; }
        public string Title { get; set; }
        public List<DenominationVm> Denominations { get; set; }

        public AtmDenominationsVm() {
            Denominations = new List<DenominationVm>();
        }
    }
}