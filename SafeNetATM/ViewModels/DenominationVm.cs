﻿namespace SafeNetATM.ViewModels {
    public class DenominationVm {
        public int Value { get; set; }
        public int Count { get; set; }
    }
}