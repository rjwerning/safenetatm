﻿using SafeNetATM.Models;
using System.Linq;

namespace SafeNetATM.Interfaces {
    public interface IRepository<T> where T : BaseModel {
        IQueryable<T> Collection();
        void Commit();
        void Delete(int Id);
        T Find(int Id);
        void Insert(T t);
        void Update(T t);
    }
}