﻿using SafeNetATM.Helpers;
using SafeNetATM.Models;
using SafeNetATM.Repository;
using SafeNetATM.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace SafeNetATM.Controllers {
    public class ATMController : Controller {
        private DenominationRepository _denonminationRepository;

        public ATMController() {
            _denonminationRepository = new DenominationRepository();
        }

        public ActionResult Index() {
            return View();
        }

        [HttpGet]
        public ActionResult ProcessRequest(string value) {
            List<string> parts = value.Split(' ').ToList();

            if (parts.Count > 0) {
                // Browsers prevent you from closing a window that a script didn't open, so "Q"uit handled in UI
                //  since results for the calls here are rendered in the result div
                switch (parts[0].ToUpper()) {
                    case "R":
                        // restock requires a single parameter
                        if (parts.Count > 1)
                            break;
                        return Restock();
                    case "W":
                        // withdraw requires 2 parameters
                        if (parts.Count != 2)
                            break;
                        return Withdraw(parts[1]);
                    case "I":
                        if (parts.Count < 2)
                            break;

                        parts.RemoveAt(0);
                        return GetBalance(parts);
                    //case "Q":
                    //    // Browsers prevent you from closing a window that a script didn't open, so just redirect to home page
                    //    return RedirectToAction("Index", "Home");
                }
            }

            return InvalidCommand(); 
        }

        private JsonResult InvalidCommand() {
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { success = false, responseText = "Invalid Command" }, JsonRequestBehavior.AllowGet);
        }

        private ActionResult Restock() {
            var result = new AtmDenominationsVm();

            var denominations = _denonminationRepository.Collection().OrderByDescending(x => x.Value).ToList();
            DenominationUtils.Restock(denominations);
            _denonminationRepository.Commit();

            AddRemainingBalance(result, denominations);

            return PartialView("_Restock", result);
        }

        private ActionResult Withdraw(string amount) {
            if (!int.TryParse(amount.Replace("$", ""), out int remainder))
                return InvalidCommand();

            var result = new AtmDenominationsVm {
                Result = $"Success: Dispensed {amount}"
            };

            var denominations = _denonminationRepository.Collection().OrderByDescending(x => x.Value).ToList();
            
            // Don't have a way to do a rollback for the memory table, so updates to the remainingqty end up
            // modifying the actual value even if it's going to fail. 
            if (!DenominationUtils.HasEnoughFunds(denominations, remainder)) {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { success = false, responseText = "Failure: insufficient funds" }, JsonRequestBehavior.AllowGet);
            }

            // This raises the question, what if 2 people try to perform a withdraw at the same time? 
            //  Not an issue for an ATM, but would need a Pessimistic way to handle this for other types of implementations
            DenominationUtils.Withdraw(denominations, ref remainder);
                           
            _denonminationRepository.Commit();

            AddRemainingBalance(result, denominations);
            return PartialView("_Withdraw", result);
        }

        private void AddRemainingBalance(AtmDenominationsVm viewModel, List<Denomination> denominations) {
            viewModel.Title = "Machine balance:";

            foreach (Denomination item in denominations)
                viewModel.Denominations.Add(new DenominationVm { Value = item.Value, Count = item.RemainingQty });
        }

        private ActionResult GetBalance(List<string> denominations) {
            var result = new AtmDenominationsVm();
            foreach (string value in denominations) {
                if (int.TryParse(value.Replace("$", ""), out int x)) {
                    var item = _denonminationRepository.GetByValue(x);
                    if (item != null)
                        result.Denominations.Add(new DenominationVm { Value = item.Value, Count = item.RemainingQty });
                    else
                        // If we can't find the value, there's something wrong with the request string
                        return InvalidCommand();
                } else {
                    // If we can't find the value, there's something wrong with the request string
                    return InvalidCommand();
                }
            }
            return PartialView("_Balance", result);
        }
    }
}