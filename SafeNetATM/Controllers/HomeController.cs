﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SafeNetATM.Controllers {
    public class HomeController : Controller {
        public ActionResult Index() {
            return View();
        }

        public ActionResult About() {
            ViewBag.Message = "SafeNet ATM development problem";
            return View();
        }

        public ActionResult Contact() {
            return View();
        }
    }
}