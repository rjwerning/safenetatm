﻿using SafeNetATM.Interfaces;
using SafeNetATM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace SafeNetATM.Repository {
    public class BaseRepository<T> : IRepository<T> where T : BaseModel {
        ObjectCache cache = MemoryCache.Default;
        List<T> items;
        string className;

        public BaseRepository() {
            className = typeof(T).Name;
            items = cache[className] as List<T>;
            if (items == null) {
                items = new List<T>();
            }
        }

        public void Commit() {
            cache[className] = items;
        }

        public void Insert(T t) {
            t.Id = items.Count + 1;
            items.Add(t);
        }

        public void Update(T t) {
            T tToUpdate = items.Find(i => i.Id == t.Id);

            if (tToUpdate != null) {
                tToUpdate = t;
            } else {
                throw new Exception(className + " not found");
            }
        }

        public T Find(int Id) {
            return items.Find(i => i.Id == Id);
        }

        public IQueryable<T> Collection() {
            return items.AsQueryable();
        }

        public void Delete(int Id) {
            T tToDelete = items.Find(i => i.Id == Id);

            if (tToDelete != null) {
                items.Remove(tToDelete);
            } else {
                throw new Exception(className + " not found");
            }
        }
    }
}