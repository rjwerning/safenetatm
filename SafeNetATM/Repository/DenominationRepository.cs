﻿using SafeNetATM.Interfaces;
using SafeNetATM.Models;
using System.Linq;

namespace SafeNetATM.Repository {
    public class DenominationRepository : BaseRepository<Denomination>, IRepository<Denomination> {
        public DenominationRepository(): base() {
            if (!Collection().Any()) {
                Insert(new Denomination { Value = 1, InitialQty = 10, RemainingQty = 10 });
                Insert(new Denomination { Value = 5, InitialQty = 10, RemainingQty = 10 });
                Insert(new Denomination { Value = 10, InitialQty = 10, RemainingQty = 10 });
                Insert(new Denomination { Value = 20, InitialQty = 10, RemainingQty = 10 });
                Insert(new Denomination { Value = 50, InitialQty = 10, RemainingQty = 10 });
                Insert(new Denomination { Value = 100, InitialQty = 10, RemainingQty = 10 });
            }
        }

        public Denomination GetByValue(int value) {
            return Collection().FirstOrDefault(x => x.Value == value);
        }
    }
}